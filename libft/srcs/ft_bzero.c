void	ft_bzero(char *s, size_t n)
{
	ft_memset(s, 0, n);
}
