#include <string.h>
#include "ft.h"

int	ft_strequ(const char *s1, const char *s2)
{

	if (!ft_strcmp(s1, s2))
		return (1);
	return (0);
}
